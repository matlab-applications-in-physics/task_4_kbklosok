%MATLAB 2020b
%name: countPulses.m
%author: kbklosok
%date: 5.12.2020
%version: 1

function [all_N, edges, num_of_pulses] = countPulses(filename, histogram_edges, bias_value)
    %Can be used to calculate histogram bin edges and bin counts
    
    file_id = fopen(filename);
    
    %maximum data processed at once
    max_data_size = 10^6;

    %alocating space
    average_data = zeros(100,500);
    
    %Change to adjust sensitivity
    acceptable_level_average_for_500_samples = 2.25;
    acceptable_level_average_for_50_samples = 25;
    
    num_of_pulses = 0;
    all_N = 0;
    
    %Iterators
    i = 0;
    j = 1;
    while ~feof(file_id)
        fseek(file_id, max_data_size*i, 'bof');
        data = fread(file_id, max_data_size, 'uint8');

        [N,edges] = histcounts(data,'BinLimits',[histogram_edges(1),histogram_edges(2)],'BinWidth',1);
        all_N = all_N + N;

        %First filter
        for k = 1:500:length(data)-500
            %Calculating averages for every 500 samples
            average_for_500_samples = sum((data(k:k+499)-bias_value))/numel(data(k:k+499));
            if average_for_500_samples > acceptable_level_average_for_500_samples
                average_data(j,1:500) = data(k:k+499)-bias_value;
                j = j + 1;
            end
        end

        i = i + 1;
    end
    fclose(file_id);
    
    %Second filter
    %If average for 50 samples is lower than acceptable value, filter it
    for ii = 1:size(average_data, 1)
        for jj = 1:50:length(average_data)
           average_for_50_samples = sum(average_data(ii, jj:jj+49))/numel(average_data(ii, jj:jj+49)); 
           if average_for_50_samples < acceptable_level_average_for_50_samples
               %Needed to show filtred pulses
               average_data(ii, jj:jj+49) = zeros(1, 50);
           else
               num_of_pulses = num_of_pulses + 1;
           end
        end
    end
    
    % uncomment to show filtered pulses
%     for m = 1:size(average_data, 1)
%     plot(average_data(m,:));
%     if sum(average_data(m,:)) > 0
%         pause;
%     end
%     end
    
end