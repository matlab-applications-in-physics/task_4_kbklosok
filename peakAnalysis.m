%MATLAB 2020b
%name: peakAnalysis.m
%author: kbklosok
%date: 5.12.2020
%version: 1
clc; clear;

%All calculations may take up to a few minutes

%Data minimum value = 46; maximum value = 192
histogram_edges = [40.5,200.5];

%Data must be in the same folder
filename = 'Co60_1350V_x20_p7_sub.dat';
filename_no_ext = replace(filename, ".dat", "");

%Get histogram bin counts and bin egdes
[all_N, edges] = get_histogram_values(filename, histogram_edges);

%Calculate bias value
bias_value = bias(filename, histogram_edges, all_N);

%Count pulses
[~,~, num_of_pulses] = countPulses(filename, histogram_edges, bias_value);

%Show histogram and save to file
fig_name = filename_no_ext + "_histogram";
hist = figure("Name", fig_name);
histogram('BinEdges', edges, 'BinCounts', all_N);
xlabel('Value')
ylabel('Count')
saveas(gcf,fig_name, 'pdf');

%Write results to file
result_filename = filename_no_ext+"_peak_analysis_results.dat";
fid = fopen(result_filename, "w");
fprintf(fid, "FileName: %s", result_filename);
fprintf(fid, "\nBiasValue: %.2f", bias_value);
fprintf(fid, "\nNumberOfPulses: %.0f", num_of_pulses);
fclose('all');

function [all_N, edges] = get_histogram_values(filename, histogram_edges)
    %Returns all bin edges and bin counts

    file_id = fopen(filename);
    max_data_size = 10^6;
    all_N = 0;

    i = 0;
    while ~feof(file_id)
    fseek(file_id, max_data_size*i, 'bof');
    data = fread(file_id, max_data_size, 'uint8');

    [N,edges] = histcounts(data,'BinLimits',[histogram_edges(1),histogram_edges(2)],'BinWidth',1);
    all_N = all_N + N;

    i = i + 1;
    end

    fclose(file_id);
end








