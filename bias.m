%MATLAB 2020b
%name: bias.m
%author: kbklosok
%date: 5.12.2020
%version: 1
function [bias_value] = bias(filename, histogram_edges, all_N)
    %Calculates bias value - average from all the most numerous values.
    arguments
       filename string
       histogram_edges double = [40.5,200.5]
       all_N double = zeros(1,length(histogram_edges(1):1:histogram_edges(2))-1)
       
    end
    
    edges = histogram_edges(1):1:histogram_edges(2);
    %If not bin counts data given
    if all_N == zeros(1,length(edges)-1)
    [all_N, edges] = get_histogram_values(filename, histogram_edges);
    end

    %Calculate Bias
    value_deviation = 3;
    bias_sum = 0;
    %Find the most numerous value in bin counts
    max_N_value = find(all_N == max(all_N));
    for kk = max_N_value-value_deviation:max_N_value+value_deviation
        %sum all values in range (the most numerous value ± value deviation)
        bias_sum = bias_sum + all_N(kk)*(edges(kk+1)-0.5);
    end
    %Calculate average
    bias_value = bias_sum/sum(all_N(max_N_value-value_deviation:max_N_value+value_deviation));
end

function [all_N, edges] = get_histogram_values(filename, histogram_edges)
    %Returns all bin edges and bin counts between histogram edges
    file_id = fopen(filename);
    max_data_size = 10^6;
    all_N = 0;

    i = 0;
    while ~feof(file_id)
    fseek(file_id, max_data_size*i, 'bof');
    data = fread(file_id, max_data_size, 'uint8');

    [N,edges] = histcounts(data,'BinLimits',[histogram_edges(1),histogram_edges(2)],'BinWidth',1);
    all_N = all_N + N;

    i = i + 1;
    end

    fclose(file_id);
end
